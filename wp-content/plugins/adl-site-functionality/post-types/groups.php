<?php

function groups_init() {
	register_post_type( 'groups', array(
		'labels'            => array(
			'name'                => __( 'Groups', 'adl-site-functionality' ),
			'singular_name'       => __( 'Groups', 'adl-site-functionality' ),
			'all_items'           => __( 'All Groups', 'adl-site-functionality' ),
			'new_item'            => __( 'New Groups', 'adl-site-functionality' ),
			'add_new'             => __( 'Add New', 'adl-site-functionality' ),
			'add_new_item'        => __( 'Add New Groups', 'adl-site-functionality' ),
			'edit_item'           => __( 'Edit Groups', 'adl-site-functionality' ),
			'view_item'           => __( 'View Groups', 'adl-site-functionality' ),
			'search_items'        => __( 'Search Groups', 'adl-site-functionality' ),
			'not_found'           => __( 'No Groups found', 'adl-site-functionality' ),
			'not_found_in_trash'  => __( 'No Groups found in trash', 'adl-site-functionality' ),
			'parent_item_colon'   => __( 'Parent Groups', 'adl-site-functionality' ),
			'menu_name'           => __( 'Groups', 'adl-site-functionality' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'thumbnail', 'revisions' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-admin-post',
		'show_in_rest'      => true,
		'rest_base'         => 'groups',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'groups_init', 1 );

function groups_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['groups'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Groups updated. <a target="_blank" href="%s">View Groups</a>', 'adl-site-functionality'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'adl-site-functionality'),
		3 => __('Custom field deleted.', 'adl-site-functionality'),
		4 => __('Groups updated.', 'adl-site-functionality'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Groups restored to revision from %s', 'adl-site-functionality'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Groups published. <a href="%s">View Groups</a>', 'adl-site-functionality'), esc_url( $permalink ) ),
		7 => __('Groups saved.', 'adl-site-functionality'),
		8 => sprintf( __('Groups submitted. <a target="_blank" href="%s">Preview Groups</a>', 'adl-site-functionality'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Groups scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Groups</a>', 'adl-site-functionality'),
		// translators: Publish box date format, see https://secure.php.net/manual/en/function.date.php
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Groups draft updated. <a target="_blank" href="%s">Preview Groups</a>', 'adl-site-functionality'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'groups_updated_messages' );
