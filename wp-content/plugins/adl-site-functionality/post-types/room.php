<?php

function room_init() {
	register_post_type( 'room', array(
		'labels'            => array(
			'name'                => __( 'Rooms', 'adl-site-functionality' ),
			'singular_name'       => __( 'Room', 'adl-site-functionality' ),
			'all_items'           => __( 'All Rooms', 'adl-site-functionality' ),
			'new_item'            => __( 'New Room', 'adl-site-functionality' ),
			'add_new'             => __( 'Add New', 'adl-site-functionality' ),
			'add_new_item'        => __( 'Add New Room', 'adl-site-functionality' ),
			'edit_item'           => __( 'Edit Room', 'adl-site-functionality' ),
			'view_item'           => __( 'View Room', 'adl-site-functionality' ),
			'search_items'        => __( 'Search Rooms', 'adl-site-functionality' ),
			'not_found'           => __( 'No Rooms found', 'adl-site-functionality' ),
			'not_found_in_trash'  => __( 'No Rooms found in trash', 'adl-site-functionality' ),
			'parent_item_colon'   => __( 'Parent Room', 'adl-site-functionality' ),
			'menu_name'           => __( 'Rooms', 'adl-site-functionality' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'thumbnail', 'revisions' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-admin-post',
		'show_in_rest'      => true,
		'rest_base'         => 'room',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'room_init', 1 );

function room_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['room'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Room updated. <a target="_blank" href="%s">View Room</a>', 'adl-site-functionality'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'adl-site-functionality'),
		3 => __('Custom field deleted.', 'adl-site-functionality'),
		4 => __('Room updated.', 'adl-site-functionality'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Room restored to revision from %s', 'adl-site-functionality'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Room published. <a href="%s">View Room</a>', 'adl-site-functionality'), esc_url( $permalink ) ),
		7 => __('Room saved.', 'adl-site-functionality'),
		8 => sprintf( __('Room submitted. <a target="_blank" href="%s">Preview Room</a>', 'adl-site-functionality'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Room scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Room</a>', 'adl-site-functionality'),
		// translators: Publish box date format, see https://secure.php.net/manual/en/function.date.php
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Room draft updated. <a target="_blank" href="%s">Preview Room</a>', 'adl-site-functionality'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'room_updated_messages' );
