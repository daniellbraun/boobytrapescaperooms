<?php

/**
 * Publisher Role
 * Changes the Editor name to Publisher. Enhances Editor Role to site to allow user ability to modify the menu. Also removes unnecessary menu options.
 *
 * from Original Author: Vladimir Garagulya
 *
 * @package vi-publisher-role
 */
class AppearancePermissions {
	/**
	 * Roles to which allow the Appearance menu.
	 *
	 * @var array $allowed_roles
	 */
	private $allowed_roles = array( 'editor' );

	/**
	 * Allowed items from Appearance menu.
	 *
	 * @var array $allowed_items
	 */
	private $allowed_items = array(
		'widgets.php',
		'nav-menus.php',
	);

	/**
	 * Capabilities required by WordPress to access to the menu items above.
	 *
	 * @var array $required_capabilities
	 */
	private $required_capabilities = array(
		'edit_theme_options',
	);

	/**
	 * Items that will prohibited from view (set later on)
	 *
	 * @var array $prohibited_items
	 */
	private $prohibited_items = array();

	public function __construct() {
		add_filter( 'user_has_cap', array( $this, 'add_required_caps' ), 10, 4 );
		add_action( 'admin_head', array( $this, 'remove_appearance_menu_items' ), 10 );
		add_action( 'admin_head', array( $this, 'appearance_redirect' ), 10 );
		add_action( 'init', array( $this, 'wps_change_role_name' ) );
		add_action( 'admin_init', array( $this, 'editor_remove_menu_pages' ) );

		// Allow Publishers to edit/manage all users except for Administrators.
		add_action( 'admin_init', array( $this, 'editor_allow_edit_users' ) );
		add_filter( 'map_meta_cap', array( $this, 'no_edit_admin' ), 10, 4 );
		add_filter( 'editable_roles', array( $this, 'remove_admin_from_roles' ) );
	}

	/**
	 * Changes Editor name to Publisher.
	 * Original Author: Raelene Morey
	 * Edited By: Bruce Brotherton - Vector & Ink
	 * @link: https://premium.wpmudev.org/blog/change-wordpress-role-names/
	 */
	function wps_change_role_name() {
		global $wp_roles;
		if ( ! isset( $wp_roles ) ) {
			$wp_roles = new WP_Roles();
		}
		$wp_roles->roles['editor']['name'] = 'Publisher';
		$wp_roles->role_names['editor']    = 'Publisher';
	}

	/**
	 * Removes menu pages that are not necessary for the publisher.
	 *
	 * Comment/uncomment these as needed.
	 *
	 * Original Author: Jean Galea
	 * Edited By: Bruce Brotherton - Vector & Ink
	 * @link: https://www.wpmayor.com/how-to-remove-menu-items-in-admin-depending-on-user-role/
	 */
	function editor_remove_menu_pages() {
		if ( ! current_user_can( 'manage_options' ) && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) {
			// remove_menu_page( 'edit.php' );                           // Posts
			// remove_menu_page( 'upload.php' );                         // Media
			// remove_menu_page( 'link-manager.php' );                   // Links
			remove_menu_page( 'edit-comments.php' );                  // Comments
			// remove_menu_page( 'edit.php?post_type=page' );            // Pages
			remove_menu_page( 'plugins.php' );                           // Plugins
			// remove_menu_page( 'themes.php' );                         // Appearance
			// remove_menu_page( 'users.php' );                          // Users
			remove_menu_page( 'tools.php' );                             // Tools
			remove_menu_page( 'options-general.php' );                   // Settings
			remove_submenu_page( 'themes.php', 'widgets.php' );          // Widgets
		}
	}


	/**
	 * Checks if current user is Editor/Publisher, and gives them capability to manage
	 * users if they don't already have it.
	 */
	function editor_allow_edit_users() {
		if ( current_user_can( 'edit_posts' ) && ! current_user_can( 'manage_options' ) ) {
			if ( ! current_user_can( 'edit_users' ) ) {
				$publisher = get_role( 'editor' );
				$publisher->add_cap( 'edit_users' );
				$publisher->add_cap( 'delete_users' );
				$publisher->add_cap( 'create_users' );
				$publisher->add_cap( 'list_users' );
				$publisher->add_cap( 'remove_users' );
				$publisher->add_cap( 'add_users' );
				$publisher->add_cap( 'promote_users' );
			}
		}
	}


	/**
	 * Remove 'Administrator' from the list of roles if the current user is not an admin.
	 * @param $roles
	 * @return mixed
	 */
	function remove_admin_from_roles( $roles ) {
		if ( isset( $roles['administrator'] ) && ! current_user_can( 'administrator' ) ) {
			unset( $roles['administrator'] );
		}

		return $roles;
	}


	/**
	 * Disallow non-admin users from editing admin users.
	 *
	 * @param $caps
	 * @param $cap
	 * @param $user_id
	 * @param $args
	 *
	 * @return array
	 */
	function no_edit_admin( $caps, $cap, $user_id, $args ) {
		switch ( $cap ) {
			case 'edit_user':
			case 'remove_user':
			case 'promote_user':
				if ( isset( $args[0] ) && $args[0] == $user_id ) {
					break;
				} elseif ( ! isset( $args[0] ) ) {
					$caps[] = 'do_not_allow';
				}
				$other = new WP_User( absint( $args[0] ) );
				if ( $other->has_cap( 'administrator' ) ) {
					if ( ! current_user_can( 'administrator' ) ) {
						$caps[] = 'do_not_allow';
					}
				}
				break;
			case 'delete_user':
			case 'delete_users':
				if ( ! isset( $args[0] ) ) {
					break;
				}
				$other = new WP_User( absint( $args[0] ) );
				if ( $other->has_cap( 'administrator' ) ) {
					if ( ! current_user_can( 'administrator' ) ) {
						$caps[] = 'do_not_allow';
					}
				}
				break;
			default:
				break;
		}

		return $caps;
	}


	/**
	 * Checks to see if user has allowed role.
	 *
	 * @param mixed $user User id.
	 *
	 * @return bool
	 */
	protected function user_has_allowed_role( $user ) {
		foreach ( $user->roles as $role ) {
			if ( in_array( $role, $this->allowed_roles ) ) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Adds the required capabilites to see the menu in appearance section.
	 *
	 * @param mixed $allcaps All Capabilities.
	 * @param mixed $caps    Capabilities.
	 * @param mixed $args    Arguments.
	 * @param mixed $user    User.
	 *
	 * @return mixed
	 */
	public function add_required_caps( $allcaps, $caps, $args, $user ) {
		if ( ! $this->user_has_allowed_role( $user ) ) {
			return $allcaps;
		}

		remove_filter( 'user_has_cap', array( $this, 'add_required_caps' ), 10, 4 );
		foreach ( $this->required_capabilities as $cap ) {
			if ( ! $user->has_cap( $cap ) ) {
				$allcaps[ $cap ] = true;
			}
		}
		add_filter( 'user_has_cap', array( $this, 'add_required_caps' ), 10, 4 );

		return $allcaps;
	}

	/**
	 * Hide other appearance menu items.
	 */
	public function remove_appearance_menu_items() {
		global $current_user, $menu, $submenu;

		if ( ! $this->user_has_allowed_role( $current_user ) ) {
			return;
		}

		if ( ! isset( $submenu['themes.php'] ) ) {
			return;
		}

		// Remove not allowed menu items under Appearance menu.
		foreach ( $submenu['themes.php'] as $key => $item ) {
			if ( ! in_array( $item[2], $this->allowed_items ) ) {
				$this->prohibited_items[] = $item[2];
				unset( $submenu['themes.php'][ $key ] );
			}
		}

		// Remove all other menu items which could become available after adding capabilities to the role.
		foreach ( $menu as $key => $item ) {
			if ( 'themes.php' === $item[2] ) { // skip 'Appearance' menu item.
				continue;
			}
			if ( in_array( $item[1], $this->required_capabilities ) ) {
				$this->prohibited_items[] = $item[2];
				unset( $menu[ $key ] );
			}
		}

		foreach ( $submenu as $sub_key => $subitem ) {
			if ( 'themes.php' === $sub_key ) { // Skip themes submenu.
				continue;
			}
			foreach ( $subitem as $key => $item ) {
				if ( in_array( $item[1], $this->required_capabilities ) ) {
					$this->prohibited_items[] = $item[2];
					unset( $submenu[ $sub_key ][ $key ] );
				}
			}
			if ( 0 === count( $subitem ) ) {
				unset( $submenu[ $sub_key ] );
			}
		}
	}

	/**
	 * Appearance redirect.
	 *
	 * Redirects user from appearance page if their role isn't in $this->user_has_allowed_role.
	 */
	public function appearance_redirect() {
		global $current_user;

		if ( ! $this->user_has_allowed_role( $current_user ) ) {
			return;
		}

		foreach ( $this->prohibited_items as $item ) {
			$result = stripos( $_SERVER['REQUEST_URI'], $item );
			if ( false !== $result ) {
				wp_redirect( get_option( 'siteurl' ) . '/wp-admin/index.php' );
			}
			break;
		}
	}
}

new AppearancePermissions();
