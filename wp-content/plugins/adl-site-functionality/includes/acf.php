<?php

namespace VectorAndInk;

/**
 * Register functions related to ACF.
 *
 * All functions using ACF hooks and functions should be defined here.
 *
 * @since      0.5.0
 * @package    VI_Site_Functionality
 * @subpackage VI_Site_Functionality/includes
 */

// Enqueue admin scripts/styles.
add_action( 'acf/input/admin_enqueue_scripts', __NAMESPACE__ . '\enqueue_acf_admin_scripts' );


/**
 * Enqueue scripts/styles on admin pages where ACF fields are rendered.
 */
function enqueue_acf_admin_scripts() {
	wp_enqueue_script( 'acf-admin-script', plugin_dir_url( dirname( __FILE__ ) ) . 'assets/js/vi-acf-admin.js', false, '1.0.0' );
}

