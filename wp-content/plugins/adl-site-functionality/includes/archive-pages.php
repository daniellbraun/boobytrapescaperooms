<?php

namespace VectorAndInk;
use WP_Post_Type;
use WP_Admin_Bar;

/**
 * Defines custom rewrite rules for estate_property posts and related
 * taxonomies.
 *
 * @since      0.5.0
 * @package    VI_Site_Functionality
 * @subpackage VI_Site_Functionality/includes
 */

/**
 * Initialize class and add our actions and hooks.
 */

// Add options page (after ACF is loaded).
add_action( 'plugins_loaded', __NAMESPACE__ . '\\register_options' );

// Make sure the CPTs are being registered with a higher priority so they are loaded FIRST.
add_action( 'acf/init', __NAMESPACE__ . '\\register_archive_group', 10 );

add_action( 'admin_bar_menu', __NAMESPACE__ . '\\add_admin_bar_links', 99 );

add_action( 'admin_notices', __NAMESPACE__ . '\\archive_page_notices' );


/**
 * Displays notices on pages that are selected as archive pages.
 */
function archive_page_notices() {
	$screen = get_current_screen();

	// Check that we're on a 'page' screen.
	if ( $screen->id === 'page' && isset( $_GET['post'] ) ) {

		// Get all custom post types.
		$post_types = get_post_types( array(
			'_builtin' => false,
		), 'objects' );

		foreach ( $post_types as $post_type ) {
			$curr_page_id    = $_GET['post'];
			$archive_page_id = get_field( $post_type->name . '_archive', 'option' );

			// If our current page ID is selected as an archive page.
			if ( $curr_page_id == $archive_page_id ) {
				$class   = 'notice notice-info is-dismissible';
				$message = 'This page is assigned as the archive page for <a href="' . get_post_type_archive_link( $post_type->name ) . '">' . $post_type->label . '</a> (' . $post_type->name . ').';
				printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message . '' );

				$p            = get_post( $archive_page_id );
				$archive_slug = is_string( $post_type->has_archive ) ? $post_type->has_archive : $post_type->rewrite['slug'];

				// Warning if the archive page & curr page slugs don't match.
				if ( $p->ID == $curr_page_id && $p->post_name !== $archive_slug ) {
					$class   = 'notice notice-error';
					$message = 'The permalink for this page (' . $p->post_name . ') doesn\'t match the ' . $post_type->label . ' archive slug (' . $archive_slug . ') don\'t match. To avoid duplicate content change the page slug to <b>' . $archive_slug . '</b>.';
					printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message );
				}
			}
		}
	}
}

/**
 * Register our archives settings page.
 */
function register_options() {
	if ( function_exists( 'acf_add_options_page' ) ) {
		acf_add_options_page( array(
			'page_title' => 'Archive Settings',
			'menu_title' => 'Archive Settings',
			'menu_slug'  => 'archive-settings',
			'capability' => 'edit_posts',
			'redirect'   => false,
		) );
	}
}

/**
 * Generate a new field group for each CPT
 *
 * @param WP_Post_Type $post_type
 */
function add_archive_group( $post_type ) {
	if ( function_exists( 'acf_add_local_field_group' ) ) {
		acf_add_local_field_group( array(
			'key'             => 'group_archive_' . $post_type->name,
			'title'           => $post_type->label . ' (' . $post_type->name . ')',
			'fields'          => array(
				array(
					'key'               => 'field_' . $post_type->name . '_archive',
					'label'             => 'Select a page to use as archive:',
					'name'              => $post_type->name . '_archive',
					'type'              => 'post_object',
					'instructions'      => 'Make sure the slug of the post being selected matches the slug of the archive page.',
					'required'          => 0,
					'conditional_logic' => 0,
					'post_type'         => array(
						0 => 'page',
					),
					'taxonomy'          => array(),
					'allow_null'        => 0,
					'multiple'          => 0,
					'return_format'     => 'id',
					'ui'                => 1,
				),
			),
			'location'        => array(
				array(
					array(
						'param'    => 'options_page',
						'operator' => '==',
						'value'    => 'archive-settings',
					),
				),
			),
			'description'     => 'Fields for choosing Ad Locations.',
			'label_placement' => 'left',
		) );
	}
}

/**
 * If there is a value set in the ACF, create a new 'Edit Archive Page' button to
 * the admin bar.
 *
 * @param WP_Admin_Bar $wp_admin_bar
 */
function add_admin_bar_links( $wp_admin_bar ) {
	$post_types = get_post_types( array(
		'_builtin' => false,
	), 'objects' );

	foreach ( $post_types as $post_type ) {
		// Get value for this post type.
		$archive_page_id = get_field( $post_type->name . '_archive', 'option' );

		// Show side bar if an archive page is selected for the current post type.
		if ( $archive_page_id && is_post_type_archive( $post_type->name ) ) {
			// Add edit page link to archives.
			$wp_admin_bar->add_node( array(
				'id'    => 'edit',
				'title' => 'Edit Archive Page',
				'href'  => admin_url( 'post.php?post=' . $archive_page_id . '&action=edit' ),
				'meta'  => array(
					'class' => 'ab-item',
				),
			) );
		}
	}
}

/**
 * Register new fields for each archive group.
 */
function register_archive_group() {
	$post_types = get_post_types( array(
		'_builtin' => false,
	), 'objects' );

	foreach ( $post_types as $post_type ) {
		// Register fields with ACF, using our generated fields.
		add_archive_group( $post_type );
	}
}
