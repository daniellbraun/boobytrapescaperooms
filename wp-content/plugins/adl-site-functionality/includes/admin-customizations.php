<?php

namespace VectorAndInk;

/**
 * Register custom post types.
 *
 * @since      0.5.0
 * @package    VI_Site_Functionality
 * @subpackage VI_Site_Functionality/includes
 */

/**
 * Initialize class and add our actions and hooks.
 */

// Enqueue admin scripts/styles
add_action( 'admin_enqueue_scripts', __NAMESPACE__ . '\\enqueue_admin_scripts' );
add_action( 'login_enqueue_scripts', __NAMESPACE__ . '\\enqueue_login_style' );

// Login page logo link & title.
add_filter( 'login_headerurl', __NAMESPACE__ . '\\login_headerurl' );
add_filter( 'login_headertitle', __NAMESPACE__ . '\\login_headertitle' );

// Change Yoast meta box priority, so it gets moved to bottom of all admin pages.
add_filter( 'wpseo_metabox_prio', function () {
	return 'low';
} );

add_action( 'admin_menu',  __NAMESPACE__ . '\\remove_menus' );
	
/**
 * Changes the logo link on the login page.
 *
 * @return string
 */
function login_headerurl() {
	return home_url();
}

/**
 * Changes the title for the logo on the login page.
 *
 * @return string
 */
function login_headertitle() {
	return get_bloginfo( 'name' );
}

/**
 * Enqueue scripts/styles for the admin.
 */
function enqueue_admin_scripts() {
	wp_enqueue_script( 'vi-admin-script', plugin_dir_url( dirname( __FILE__ ) ) . 'assets/js/vi-admin.js', false, '1.0.0' );
}

/**
 * Enqueue style for login page.
 */
function enqueue_login_style() {
	wp_enqueue_style( 'login-style', plugin_dir_url( dirname( __FILE__ ) ) . 'assets/css/vi-admin-login.css', '', '1.0.0', 'all' );
}


/**
 * Remove unnecessary menu items from admin menu for this site
 */
function remove_menus() {
	remove_menu_page( 'edit.php' ); //Posts
	remove_menu_page( 'edit-comments.php' );
	remove_menu_page( 'tools.php' );
}