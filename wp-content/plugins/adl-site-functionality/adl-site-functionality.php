<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin and
 * registers the activation and deactivation functions.
 *
 * @wordpress-plugin
 * Plugin Name:       Booby Trap Escape Rooms Site Functionality
 * Plugin URI:        https://www.vectorandink.com/
 * Description:       This plugin contains all persistent functionality for the Booby Trap Escape Rooms site.
 * Version:           0.5
 * Author:            Heather Brokmeier, Vector & Ink.
 * Author URI:        https://www.vectorandink.com/
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Load required dependencies for this plugin.
 */
$includes = [
	'includes/acf.php',                  // ACF functions.
	'includes/admin-customizations.php', // Admin customizations.
	'includes/archive-pages.php',        // Select custom archive page for each CPT.
	'includes/rewrite-rules.php',        // Custom rewrite rules.
	'post-types/room.php',
	'post-types/groups.php',
];

foreach ( $includes as $file ) {
	if ( ! $filepath = plugin_dir_path( __FILE__ ) ) {
		trigger_error( sprintf( 'Error locating %s for inclusion', $file ), E_USER_ERROR );
	}
	require_once plugin_dir_path( __FILE__ ) . $file;
}
unset( $file );
