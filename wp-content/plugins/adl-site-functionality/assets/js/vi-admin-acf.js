(function( $ ) {
    'use strict';

    /**
     * Adds ability to deep link to ACF options page tabs.
     *
     * @link https://www.justinsilver.com/technology/wordpress/acf-link-to-specific-tab/
     */
    // Run when ACF is ready.
    acf.add_action('ready', function(){
        // get everything after the #
        var hash = location.hash.substring(1);
        var acfTab = $('.acf-tab-wrap .acf-tab-button');

        // check if there is a hash
        if (location.hash.length>1){
            // loop through the tab buttons and try to find a match
            acfTab.each(function(i, button){
                if (hash==$(button).text().toLowerCase().replace(' ', '-')){
                    // if we found a match, click it then exit the each() loop
                    $(button).trigger('click');
                    return false;
                }
            });
        }

        // when a table is clicked, update the hash in the URL
        acfTab.on('click', function($el){
            location.hash='#'+$(this).text().toLowerCase().replace(' ', '-');
        });
    });

})( jQuery );
