<?php
/**
 * Template for displaying single taxonomy pages
 *
 * @package     WordPress
 * @subpackage  Timber
 * @since       Timber 0.1
 */
$context          = Timber\Timber::get_context();
$term             = new Timber\Term();
$context['post']  = $term; // Use {{ post.whatever }} to refer to the term in the twig template.
$context['posts'] = new Timber\PostQuery();

if ( 'custom-tax-1' === $term->taxonomy ) {
	// Do the stuff for custom_tax_slug_1 here.
}

if ( 'custom-tax-1' === $term->taxonomy ) {
	// Do the stuff for custom_tax_slug_2 here.
}

Timber\Timber::render( array(
	'taxonomy-' . $term->ID . '.twig',
	'taxonomy-' . $term->slug . '.twig',
	'taxonomy-' . $term->taxonomy . '.twig',
	'taxonomy.twig',
	'page.twig',
), $context );
