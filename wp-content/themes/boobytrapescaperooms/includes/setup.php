<?php

namespace VectorAndInk\Sage\Setup;

use VectorAndInk\Sage\Assets;

/**
 * Theme setup
 */
function setup() {
	// Add theme support.
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'html5', [ 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ] );

	// Register wp_nav_menu() menus here.
	register_nav_menus( [
		'primary_navigation' => 'Primary Navigation',
		'footer_navigation' => 'Footer Navigation',
	] );

	/*
	 * Use main stylesheet for visual editor.
	 *
	 * This imports the main stylesheet into the visual editor, with some
	 * fixes for Foundation styles that messed it up.
	 *
	 * To add custom styles to ONLY the editor, add them to this file.
	 */
	add_editor_style( Assets\asset_path( 'css/admin-app.css' ) );
}
add_action( 'after_setup_theme', __NAMESPACE__ . '\\setup' );


/**
 * Register sidebars.
 *
 * Delete this function if you're not using widgets.
 */
function widgets_init() {
	register_sidebar( [
		'name'          => 'Primary',
		'id'            => 'sidebar-primary',
		'before_widget' => '<section class="widget %1$s %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	] );

	register_sidebar( [
		'name'          => 'Footer',
		'id'            => 'sidebar-footer',
		'before_widget' => '<section class="widget %1$s %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>',
	] );
}
add_action( 'widgets_init', __NAMESPACE__ . '\\widgets_init' );

/**
 * Add ACF options page.
 */
function add_options_page() {
	if ( function_exists( 'acf_add_options_page' ) ) {
		acf_add_options_page( array(
			'page_title' => 'Settings',
			'menu_slug'  => 'options-page',
			'icon_url'   => 'dashicons-admin-generic',
			'redirect'   => false,
			'position'   => 80,
			'autoload'   => true, // Load all options by default.
		) );
	}
}
add_action( 'acf/init', __NAMESPACE__ . '\\add_options_page' );

/**
 * Set Wordpress option "Front page displays a static page" to Front Page: Sample Page (id 2)
 */
function set_static_front_page () {
	if ( get_option( 'show_on_front' ) != 'page' ) {
		update_option( 'page_on_front', 2 );
		update_option( 'show_on_front', 'page' );
	}
}
add_action('after_switch_theme', __NAMESPACE__ . '\\set_static_front_page');