<?php

namespace VectorAndInk\Sage\Assets;

/**
 * Get paths for assets
 */
function asset_path( $filename ) {
	$dist_path = get_template_directory_uri() . '/';
	$directory = dirname( $filename ) . '/';
	$file      = basename( $filename );
	return $dist_path . $directory . $file;
}

/**
 * Theme assets
 */
function assets() {
	/*
	 * Styles
	 */
	wp_register_style( 'app/css', asset_path( 'css/app.css' ), '1.0' );
	
	wp_enqueue_style( [
		'app/css',
	] );


	/*
	 * Scripts
	 */
	wp_register_script( 'app/js', asset_path( 'js/app.js' ), [ 'jquery' ], null, true );

	wp_enqueue_script( [
		'app/js'
	] );

	// Conditional scripts
	if ( is_single() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100 );

/**
 * Remove extra stuff from the <head>.
 *
 * If the REST API is being used, `wp_oembed_register_route` and `rest_output_link_wp_head` need
 * to be removed.
 */
function clean_up_head() {
	remove_action( 'rest_api_init', 'wp_oembed_register_route' );       // Remove oEmbed REST API route. (Remove this if using REST API.)
	remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 ); // Turn off oEmbed filtering.
	remove_action( 'wp_head', 'rest_output_link_wp_head' );             // Outputs the REST API link tag into page header. (Remove this if using REST API.)
	remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );        // Remove oEmbed discovery links.
	remove_action( 'wp_head', 'wp_oembed_add_host_js' );                // Remove oEmbed-specific JavaScript from the front-end and back-end.
	remove_action( 'wp_head', 'rsd_link' );                             // Remove really simple discovery link.
	remove_action( 'wp_head', 'feed_links', 2 );                        // Remove rss feed links (make sure you add them in yourself if youre using feedblitz or an rss service).
	remove_action( 'wp_head', 'feed_links_extra', 3 );                  // Removes all extra rss feed links.
	remove_action( 'wp_head', 'index_rel_link' );                       // Remove link to index page.
	remove_action( 'wp_head', 'wlwmanifest_link' );                     // Remove wlwmanifest.xml (needed to support windows live writer).
	remove_action( 'wp_head', 'wp_generator' );                         // Remove wordpress version.
	remove_action( 'wp_head', 'start_post_rel_link', 10 );              // Remove random post link.
	remove_action( 'wp_head', 'parent_post_rel_link', 10 );             // Remove parent post link.
	remove_action( 'wp_head', 'adjacent_posts_rel_link', 10 );          // Remove the next and previous post links.
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10 );
	remove_action( 'wp_head', 'wp_shortlink_wp_head', 10 );
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
}
add_action( 'init',  __NAMESPACE__ . '\\clean_up_head', 9999 );
