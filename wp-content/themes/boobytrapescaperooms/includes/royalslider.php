<?php

namespace VectorAndInk\RoyalSlider;

use Mustache_Engine;
use Timber\Image;
use Timber\ImageHelper;

/**
 * Edit list of RoyalSlider templates. Remove all template except default, and edit the
 * default options for the default template.
 *
 * @link http://help.dimsemenov.com/discussions/royalslider-wordpress/2140-custom-default-template
 *
 * @param array $templates RoyalSlider templates.
 *
 * @return mixed
 */
function manage_royalslider_templates( $templates ) {
	// Unset other slider templates.
	unset( $templates['default'] );
	unset( $templates['gallery'] );
	unset( $templates['gallery_vertical_fade'] );
	unset( $templates['content_slider'] );
	unset( $templates['simple_vertical'] );
	unset( $templates['gallery_with_thumbs_text'] );
	unset( $templates['visible_nearby_zoom'] );
	unset( $templates['visible_nearby_simple'] );
	unset( $templates['gallery_thumbs_grid'] );
	unset( $templates['slider_rs_home'] );
	unset( $templates['slider_in_laptop'] );
	unset( $templates['two_at_once'] );

	$shared_markup = '<div>
  {{#link_url}}<a href="{{link_url}}">{{/link_url}}
     {{responsive_image_tag}}
     
     {{html}}
     
     <div class="caption">
       <h3>{{title}}</h3>
       <p>{{description}}</p>
     </div>
     
     {{animated_blocks}}
    {{#link_url}}</a>{{/link_url}}
</div>';

	$shared_options = array(
		// Size & scaling
		'width'                => '100%',
		'autoScaleSlider'      => 'false',
		'autoHeight'           => 'false',

		// Image Options
		'imageScaleMode'       => 'none',
		'imageAlignCenter'     => 'false',
		'imageScalePadding'    => 'false',

		// Thumbnails, bullets, tabs
		'controlNavigation'    => 'none',

		// Arrows
		'arrowsNav'            => 'true',
		'arrowsNavAutohide'    => 'true',
		'arrowsNavHideOnTouch' => 'true',

		// Autoplay
		'autoPlay'             => 'false',

		// Miscellaneous
		'transitionType'       => 'move',
		'transitionSpeed'      => '400',
		'loop'                 => 'false',
		'loopRewind'           => 'true',
		'numImagesToPreload'   => '0',
		'sliderDrag'           => 'true',
		'sliderTouch'          => 'true',
		'keyboardNavEnabled'   => 'true',
		'fadeinLoadedSlide'    => 'false',
		'allowCSS3'            => 'true',
		'addActiveClass'       => 'true',
	);

	// Home Slider Template
	$templates['home']['template-css-class']          = 'rs-home-template';
	$templates['home']['template-html']               = $shared_markup;
	$templates['home']['options']['sopts']            = $shared_options;
	$templates['home']['options']['sopts']['height']  = 550;
	$templates['home']['options']['image_generation'] = array(
		'lazyLoading' => 'false',
		'imageWidth'  => 1800,
		'imageHeight' => 550,
	);

	// Subpage Slider Template
	$templates['subpage']['template-css-class']          = 'rs-subpage-template';
	$templates['subpage']['template-html']               = $shared_markup;
	$templates['subpage']['options']['sopts']            = $shared_options;
	$templates['subpage']['options']['sopts']['height']  = 400;
	$templates['subpage']['options']['image_generation'] = array(
		'lazyLoading' => 'false',
		'imageWidth'  => 1800,
		'imageHeight' => 400,
	);

	return $templates;
}

add_filter( 'new_royalslider_templates', __NAMESPACE__ . '\\manage_royalslider_templates', 10, 2 );

/**
 * Add CSS styles to modify the Template icons in the "General Options" section
 * of the sliders.
 */
function royalslider_admin_styles() {
	echo '<style>
	#new-royalslider-options label {
		position: relative;
		background-position: -89px -100px !important;
	}

	#new-royalslider-options label::after,
	#new-royalslider-options label::before {
		position: absolute;
	    left: 0;
	    width: 100%;
	    text-align: center;
	}
	
	#new-royalslider-options label[for="home"]::after {
		top: 0;
		content: "Home";
	}
	
	#new-royalslider-options label[for="home"]::before {
		bottom: 0;
		content: "1800 x 550";
	}
	
	#new-royalslider-options label[for="subpage"]::after {
		top: 0;
		content: "Subpage";
	}
	
	#new-royalslider-options label[for="subpage"]::before {
		bottom: 0;
		content: "1800 x 400";
	}
}
</style>';
}

add_action( 'admin_head', __NAMESPACE__ . '\\royalslider_admin_styles' );

/**
 * Replace list of Royal Slider skins with custom one.
 *
 * @link http://help.dimsemenov.com/kb/wordpress-royalslider-advanced/wp-adding-custom-skin-without-modifying-core-of-slider
 *
 * @param $skins
 *
 * @return mixed
 */
function royalslider_add_custom_skin() {
	$skins               = array();
	$skins['customSkin'] = array(
		'label' => 'Default',
	);

	return $skins;
}

add_filter( 'new_royalslider_skins', __NAMESPACE__ . '\\royalslider_add_custom_skin', 10 );


/**
 * Hide the options on the Slider page for non-admin users.
 *
 * This hides ALL options except for "General Options", ideally we would only
 * want to hide some of the option sections, but there is not a hook or CSS to
 * target specific sections.
 */
function hide_royalslider_options() {
	global $pagenow;

	// Hide other options if user isn't Admin.
	if ( ! current_user_can( 'manage_options' )
	     && 'admin.php' === $pagenow
	     && 'new_royalslider' === $_GET['page']
	) {
		echo '<style>.other-options { display: none; }</style>';
	}
}

add_action( 'admin_head', __NAMESPACE__ . '\\hide_royalslider_options' );


/**
 * Add responsive/lazyloaded images to Royal Slider.
 *
 * @link http://help.dimsemenov.com/kb/wordpress-royalslider-advanced/wp-using-wordpress-responsive-images
 *
 * @param Mustache_Engine $m Mustache template renderer.
 * @param array           $slide_data
 * @param array           $options
 */
function add_resp_img_variable( $m, $slide_data, $options ) {

	$m->addHelper( 'responsive_image_tag', function () use ( $slide_data, $options ) {
		$attachment_id = 0;

		if ( is_object( $slide_data ) && isset( $slide_data->ID ) ) {
			$attachment_id = $slide_data->ID;
		} else if ( isset( $slide_data['image'] ) && isset( $slide_data['image']['attachment_id'] ) ) {
			$attachment_id = $slide_data['image']['attachment_id'];
		}

		if ( $attachment_id ) {
			$image  = new Image( $attachment_id );
			$height = $options['sopts']['height'];

			$slider       = ImageHelper::resize( $image->src(), 1800, $height );
			$large        = ImageHelper::resize( $image->src(), 1200, $height / ( 1800 / 1200 ) );
			$medium_large = ImageHelper::resize( $image->src(), 600, $height / ( 1800 / 600 ) );
			$medium       = ImageHelper::resize( $image->src(), 300, $height / ( 1800 / 300 ) );

			$srcset = 'data-srcset="' . $medium . ' 300w, ' . $medium_large . ' 600w, ' . $large . ' 1200w, ' . $slider . ' 1800w"';
			$video  = $slide_data['video'] ? 'data-rsVideo="' . $slide_data['video']['url'] . '"' : '';

			return '<img src="' . $medium . '" ' . $srcset . ' ' . $video . 'class="mainSlideImage blur-up lazyload" alt="" />';
		}

		return '{{ image_tag }}';
	} );
}

add_filter( 'new_rs_slides_renderer_helper', __NAMESPACE__ . '\\add_resp_img_variable', 10, 4 );


/**
 * Register ACF field group for RoyalSlider select.
 */
function register_slider_select_field() {

	if ( function_exists( 'acf_add_local_field_group' ) ) :

		acf_add_local_field_group( array(
			'key'                   => 'group_royalslider_select',
			'title'                 => 'Slider',
			'fields'                => array(
				array(
					'key'               => 'field_royalslider_select',
					'label'             => 'Choose a slider for this page:',
					'name'              => 'royalslider',
					'type'              => 'select',
					'instructions'      => '',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'choices'           => array(
						'none' => 'None',
					),
					'default_value'     => array(
						'none' => 'None',
					),
					'allow_null'        => 0,
					'multiple'          => 0,
					'ui'                => 0,
					'ajax'              => 0,
					'placeholder'       => '',
					'disabled'          => 0,
					'readonly'          => 0,
				),
			),
			'location'              => array(
				array(
					array(
						'param'    => 'post_type',
						'operator' => '==',
						'value'    => 'page',
					),
				),
			),
			'menu_order'            => 0,
			'position'              => 'side',
			'style'                 => 'default',
			'label_placement'       => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen'        => '',
			'active'                => 1,
			'description'           => 'List of Royalslider sliders',
		) );

	endif;

}

add_action( 'wp_loaded', __NAMESPACE__ . '\\register_slider_select_field' );

/**
 * Get all RoyalSliders and populate the "royalslider" ACF select field with their ID and name.
 *
 * @link http://www.advancedcustomfields.com/resources/dynamically-populate-a-select-fields-choices/
 *
 * @param array $field ACF field name.
 *
 * @return mixed
 */
function load_royalsliders( $field ) {
	if ( class_exists( 'NewRoyalSliderMain' ) ) {
		global $wpdb;

		$field['choices']       = array();
		$field['default_value'] = array();

		$field['choices']['none']       = 'None';
		$field['default_value']['none'] = 'None';

		$sliders = $wpdb->get_results( 'SELECT * FROM ' . $wpdb->prefix . 'new_royalsliders ORDER BY id' );

		foreach ( $sliders as $slider ) {
			if ( $slider->type !== 'gallery' ) {
				$field['choices'][ $slider->id ] = $slider->name;
			}
		}
	} else {
		if ( is_admin() ) {
			print_r( '<p class="description">Error: Please make sure RoyalSlider is installed and activated.</p>' );
		}
	}

	return $field;
}

add_filter( 'acf/load_field/key=field_royalslider_select', __NAMESPACE__ . '\\load_royalsliders' );
