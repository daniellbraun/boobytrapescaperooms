<?php

namespace VectorAndInk\Sage\Extras;

/**
 * Filter MCE Settings. Add custom classes to the TinyMCE 'Formats'
 * dropdwon.
 *
 * @link http://wordpress.stackexchange.com/questions/128931/tinymce-adding-css-to-format-dropdown
 *
 * @param $settings
 *
 * @return mixed
 */
function mce_custom_formats( $settings ) {
	// Remove 'H1' heading level from list.
	$settings['block_formats'] = 'Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6;Paragraph=p;';

	$style_formats = array(
		/*
		* Each array child is a format with it's own settings
		* Notice that each array has title, block, classes, and wrapper arguments
		* Title is the label which will be visible in Formats menu
		* Block defines whether it is a span, div, selector, or inline style
		* Classes allows you to define CSS classes
		* Wrapper whether or not to add a new block-level element around any selected elements
		*/
		array(
			'title'    => 'Button - Primary',
			'selector' => 'a',
			'classes'  => 'button primary',
		),
		array(
			'title'    => 'Button - Secondary',
			'selector' => 'a',
			'classes'  => 'button secondary',
		),
	);

	// Insert the array, JSON ENCODED, into 'style_formats'
	$settings['style_formats'] = json_encode( $style_formats );

	/*
	 * Allows styles to be applied to the formats dropdown.
	 * ['preview_settings'] limits which styles are allowed in the preview, removing it let them all be styled.
	 * http://wordpress.stackexchange.com/a/116790/87686
	 */
	unset( $settings['preview_styles'] );

	return $settings;
}

add_filter( 'tiny_mce_before_init', __NAMESPACE__ . '\\mce_custom_formats' );

/**
 * Add 'Formats' dropdown to TinyMCE
 *
 * @param $buttons
 *
 * @return mixed
 */
function mce_buttons_2( $buttons ) {
	array_unshift( $buttons, 'styleselect' );

	return $buttons;
}

add_filter( 'mce_buttons_2', __NAMESPACE__ . '\\mce_buttons_2' );
