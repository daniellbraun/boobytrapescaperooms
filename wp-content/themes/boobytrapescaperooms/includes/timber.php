<?php
use VectorAndInk\Sage\Setup;

/**
 * Check if Timber is activated
 */

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function () {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
	} );
	return;
}

// Define path to template files
Timber\Timber::$dirname = array( 'templates', 'views' );

/**
 * Timber
 */
class StarterTheme extends Timber\Site {
	function __construct() {
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		parent::__construct();
	}

	function add_to_context( $context ) {
		/* Add options */
		if ( function_exists( 'get_fields' ) ) {
			$context['option'] = get_fields( 'options' );
		};

		/* Menu */
		/* Add registered menus here if they need to be available on every page. */
		$context['menu'] = new Timber\Menu( 'primary_navigation' );
		$context['footer_menu'] = new Timber\Menu( 'footer_navigation' );

		/* Site info */
		$context['site'] = $this;

		/* Sidebars */
		/* Delete these if you're not using widgets. */
		$context['sidebar_primary'] = Timber\Timber::get_widgets( 'sidebar-primary' );

		$context['rooms'] = Timber\Timber::get_posts( array(
			'post_type' => 'room',
			'posts_per_page' => -1,
		) );

		return $context;
	}
}

new StarterTheme();
