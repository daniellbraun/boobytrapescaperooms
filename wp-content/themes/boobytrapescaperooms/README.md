# Vector & Ink Starter Theme
This is the starter theme for Vector & Ink WordPress projects for using [Foundation 6](http://foundation.zurb.com/sites) and [Timber](https://github.com/jarednova/timber) together.

It's a customized version of the [timber-starter-theme](https://github.com/upstatement/timber-starter-theme), using the setup from the [foundation-zurb-template](https://github.com/zurb/foundation-zurb-template). Take a look at those repos for more detailed information.

Features:

- Sass compilation and browser prefixing
- Sass sourcemaps
- JavaScript concatenation
- For production builds:
    - CSS compression
    - JavaScript compression

### Requirements
Install these first if needed.

- [git](https://git-scm.com/)
- [Node.js](https://nodejs.org/)
- [bower](http://bower.io/)
- [gulp](http://gulpjs.com/)

### Installation
#### Copy the Starter Theme
Clone the starter theme into your themes directory (`theme-name` should be your name for the new theme).

```bash
git clone https://bitbucket.org/adlit/adlit-starter-theme theme-name
```

#### Install Dependencies
Install [npm3](https://www.npmjs.com/package/npm3) globally: `npm install -g npm3` (npm3's [dependency resolution](https://docs.npmjs.com/how-npm-works/npm3) makes running commands faster)

Open the theme folder in your command line and install the needed dependencies:
```bash
npm3 install
bower install
```

#### Run
Configure BrowserSync: Open `config.yml` and change line 2 `PROXY` to your local development domain.

Run `gulp` to compile/concatenate CSS & JS and start BrowserSync.

### Usage
#### Gulp commands
- `gulp` build CSS/JS, start BrowserSync, watch for file changes
- `gulp build` build CSS/JS (non-minified, with sourcemaps)
- `gulp build --production` build CSS/JS (minified)

Compiled CSS will be output to `css/app.css` and JS to `js/app.js`.

#### BrowserSync
BrowserSync will start automatically when you run `gulp`.

Edit line 2 of `config.yml` with your local domain.

#### config.yml
`config.yml` contains the CSS file paths and the list of Foundation JS files to include, as well as BrowserSync and autoprefixer settings.