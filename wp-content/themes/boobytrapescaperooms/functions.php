<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
	'includes/timber.php',       // Twig magic.
	'includes/assets.php',       // Scripts and stylesheets.
	'includes/extras.php',       // Custom functions.
	'includes/setup.php',        // Theme and widget setup.
	'includes/royalslider.php',  // RoyalSlider functions.
];

foreach ( $sage_includes as $file ) {
	if ( ! $filepath = locate_template( $file ) ) {
		trigger_error( sprintf( 'Error locating %s for inclusion', $file ), E_USER_ERROR );
	}
	require_once $filepath;
}
unset( $file, $filepath );
