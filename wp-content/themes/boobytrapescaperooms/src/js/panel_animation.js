const PanelAnimation = (function() {
    const panels = document.querySelectorAll('.panel');
    function _toggleOpen() {
        this.classList.add('open');
    }
    function _closePanel() {
        this.classList.remove('open', 'open-active');
    }
    function init() {
        panels.forEach(panel => panel.addEventListener('mouseenter', _toggleOpen));
        panels.forEach(panel => panel.addEventListener('mouseleave', _closePanel));
    }
    return {
        init
    }
})();
