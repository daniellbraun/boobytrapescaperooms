const ShowMoreText = ( function() {
    function init() {
        const showMoreButtons = document.querySelectorAll(".show-more");
        const roomContentAll = document.querySelectorAll(".room-content");

        roomContentAll.forEach( (roomContent) => {

            const paragraphs = roomContent.querySelectorAll('p');
            let totalHeight = 0;

            paragraphs.forEach( (p) => {
                totalHeight += p.offsetHeight;
            });
            if (totalHeight < 80 ) {
                const buttonToHide = document.querySelector("#" + roomContent.id + " ~ " + ".show-more");
                buttonToHide.style.display = "none";
                console.log('buttonToHide', buttonToHide);
            }
        });
        showMoreButtons.forEach( (button) => {

            button.addEventListener("click", function (event) {

                const content = document.getElementById(event.target.dataset.content);

                if ( content.classList.contains("open") ) {
                    //shrink the box
                    button.innerHTML = "SHOW MORE";
                    content.classList.remove("open");
                } else {
                    //expand the box
                    content.classList.add("open");
                    button.innerHTML = "SHOW LESS";
                }
            })
        })
    }
    return {
        init
    }
})();